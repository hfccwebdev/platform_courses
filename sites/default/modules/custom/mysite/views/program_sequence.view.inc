<?php

/**
 * @file
 * Defines Program Sequence view.
 */

$view = new view();
$view->name = 'program_sequence';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Program Sequence';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Program Sequence';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['hide_empty'] = TRUE;
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h3';
$handler->display->display_options['fields']['title']['element_wrapper_class'] = 'title';
/* Field: Content: Sequence Note */
$handler->display->display_options['fields']['field_program_sequence_note']['id'] = 'field_program_sequence_note';
$handler->display->display_options['fields']['field_program_sequence_note']['table'] = 'field_data_field_program_sequence_note';
$handler->display->display_options['fields']['field_program_sequence_note']['field'] = 'field_program_sequence_note';
$handler->display->display_options['fields']['field_program_sequence_note']['label'] = '';
$handler->display->display_options['fields']['field_program_sequence_note']['element_label_colon'] = FALSE;
/* Field: Content: Fall Start */
$handler->display->display_options['fields']['field_program_seq_fall']['id'] = 'field_program_seq_fall';
$handler->display->display_options['fields']['field_program_seq_fall']['table'] = 'field_data_field_program_seq_fall';
$handler->display->display_options['fields']['field_program_seq_fall']['field'] = 'field_program_seq_fall';
$handler->display->display_options['fields']['field_program_seq_fall']['element_label_type'] = 'h3';
$handler->display->display_options['fields']['field_program_seq_fall']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_program_seq_fall']['element_wrapper_class'] = 'field-type-program-sequence';
$handler->display->display_options['fields']['field_program_seq_fall']['click_sort_column'] = 'term_value';
$handler->display->display_options['fields']['field_program_seq_fall']['delta_offset'] = '0';
/* Field: Content: Winter Start */
$handler->display->display_options['fields']['field_program_seq_winter']['id'] = 'field_program_seq_winter';
$handler->display->display_options['fields']['field_program_seq_winter']['table'] = 'field_data_field_program_seq_winter';
$handler->display->display_options['fields']['field_program_seq_winter']['field'] = 'field_program_seq_winter';
$handler->display->display_options['fields']['field_program_seq_winter']['element_label_type'] = 'h3';
$handler->display->display_options['fields']['field_program_seq_winter']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_program_seq_winter']['element_wrapper_class'] = 'field-type-program-sequence';
$handler->display->display_options['fields']['field_program_seq_winter']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_program_seq_winter']['click_sort_column'] = 'term_value';
$handler->display->display_options['fields']['field_program_seq_winter']['delta_offset'] = '0';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Contextual filter: Content: Program (field_program) */
$handler->display->display_options['arguments']['field_program_target_id']['id'] = 'field_program_target_id';
$handler->display->display_options['arguments']['field_program_target_id']['table'] = 'field_data_field_program';
$handler->display->display_options['arguments']['field_program_target_id']['field'] = 'field_program_target_id';
$handler->display->display_options['arguments']['field_program_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_program_target_id']['summary']['format'] = 'default_summary';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'program_sequence' => 'program_sequence',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '40';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_program_target_id']['id'] = 'field_program_target_id';
$handler->display->display_options['relationships']['field_program_target_id']['table'] = 'field_data_field_program';
$handler->display->display_options['relationships']['field_program_target_id']['field'] = 'field_program_target_id';
$handler->display->display_options['relationships']['field_program_target_id']['label'] = 'Program';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
/* Field: Content: Degree Type */
$handler->display->display_options['fields']['field_program_type']['id'] = 'field_program_type';
$handler->display->display_options['fields']['field_program_type']['table'] = 'field_data_field_program_type';
$handler->display->display_options['fields']['field_program_type']['field'] = 'field_program_type';
$handler->display->display_options['fields']['field_program_type']['relationship'] = 'field_program_target_id';
$handler->display->display_options['fields']['field_program_type']['label'] = '';
$handler->display->display_options['fields']['field_program_type']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_program_type']['alter']['text'] = '([field_program_type])';
$handler->display->display_options['fields']['field_program_type']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_program_type']['element_wrapper_type'] = 'em';
$handler->display->display_options['fields']['field_program_type']['element_default_classes'] = FALSE;
$handler->display->display_options['path'] = 'catalog/program-sequence';

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = 'Recommended Course Sequence';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nothing']['element_wrapper_type'] = 'h3';
$handler->display->display_options['fields']['nothing']['element_wrapper_class'] = 'title';
$handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
/* Field: Content: Edit link */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['element_wrapper_type'] = 'div';
$handler->display->display_options['fields']['edit_node']['element_wrapper_class'] = 'edit-link';
$handler->display->display_options['fields']['edit_node']['element_default_classes'] = FALSE;
/* Field: Content: Sequence Note */
$handler->display->display_options['fields']['field_program_sequence_note']['id'] = 'field_program_sequence_note';
$handler->display->display_options['fields']['field_program_sequence_note']['table'] = 'field_data_field_program_sequence_note';
$handler->display->display_options['fields']['field_program_sequence_note']['field'] = 'field_program_sequence_note';
$handler->display->display_options['fields']['field_program_sequence_note']['label'] = '';
$handler->display->display_options['fields']['field_program_sequence_note']['element_label_colon'] = FALSE;
/* Field: Content: Fall Start */
$handler->display->display_options['fields']['field_program_seq_fall']['id'] = 'field_program_seq_fall';
$handler->display->display_options['fields']['field_program_seq_fall']['table'] = 'field_data_field_program_seq_fall';
$handler->display->display_options['fields']['field_program_seq_fall']['field'] = 'field_program_seq_fall';
$handler->display->display_options['fields']['field_program_seq_fall']['element_label_type'] = 'h4';
$handler->display->display_options['fields']['field_program_seq_fall']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_program_seq_fall']['element_wrapper_class'] = 'field-type-program-sequence';
$handler->display->display_options['fields']['field_program_seq_fall']['click_sort_column'] = 'term_value';
$handler->display->display_options['fields']['field_program_seq_fall']['delta_offset'] = '0';
/* Field: Content: Winter Start */
$handler->display->display_options['fields']['field_program_seq_winter']['id'] = 'field_program_seq_winter';
$handler->display->display_options['fields']['field_program_seq_winter']['table'] = 'field_data_field_program_seq_winter';
$handler->display->display_options['fields']['field_program_seq_winter']['field'] = 'field_program_seq_winter';
$handler->display->display_options['fields']['field_program_seq_winter']['element_label_type'] = 'h4';
$handler->display->display_options['fields']['field_program_seq_winter']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_program_seq_winter']['element_wrapper_class'] = 'field-type-program-sequence';
$handler->display->display_options['fields']['field_program_seq_winter']['click_sort_column'] = 'term_value';
$handler->display->display_options['fields']['field_program_seq_winter']['delta_offset'] = '0';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Program (field_program) */
$handler->display->display_options['arguments']['field_program_target_id']['id'] = 'field_program_target_id';
$handler->display->display_options['arguments']['field_program_target_id']['table'] = 'field_data_field_program';
$handler->display->display_options['arguments']['field_program_target_id']['field'] = 'field_program_target_id';
$handler->display->display_options['arguments']['field_program_target_id']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_program_target_id']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['field_program_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_program_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_program_target_id']['summary_options']['items_per_page'] = '25';
