<?php

/**
 * @file
 * Defines the Offices Export view.
 */

$view = new view();
$view->name = 'offices_export';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Offices Export';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_administrator_target_id']['id'] = 'field_administrator_target_id';
$handler->display->display_options['relationships']['field_administrator_target_id']['table'] = 'field_data_field_administrator';
$handler->display->display_options['relationships']['field_administrator_target_id']['field'] = 'field_administrator_target_id';
$handler->display->display_options['relationships']['field_administrator_target_id']['label'] = 'Administrator';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'name';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Phone Number */
$handler->display->display_options['fields']['field_phone_number']['id'] = 'field_phone_number';
$handler->display->display_options['fields']['field_phone_number']['table'] = 'field_data_field_phone_number';
$handler->display->display_options['fields']['field_phone_number']['field'] = 'field_phone_number';
$handler->display->display_options['fields']['field_phone_number']['label'] = 'office-phone';
$handler->display->display_options['fields']['field_phone_number']['hide_empty'] = TRUE;
/* Field: Content: Email Address */
$handler->display->display_options['fields']['field_email_address']['id'] = 'field_email_address';
$handler->display->display_options['fields']['field_email_address']['table'] = 'field_data_field_email_address';
$handler->display->display_options['fields']['field_email_address']['field'] = 'field_email_address';
$handler->display->display_options['fields']['field_email_address']['label'] = 'email';
$handler->display->display_options['fields']['field_email_address']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['field_email_address']['type'] = 'email_plain';
/* Field: Content: Administrator */
$handler->display->display_options['fields']['field_administrator']['id'] = 'field_administrator';
$handler->display->display_options['fields']['field_administrator']['table'] = 'field_data_field_administrator';
$handler->display->display_options['fields']['field_administrator']['field'] = 'field_administrator';
$handler->display->display_options['fields']['field_administrator']['label'] = 'administrator';
$handler->display->display_options['fields']['field_administrator']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['field_administrator']['settings'] = array(
  'link' => 0,
);
/* Field: Staff directory: Title */
$handler->display->display_options['fields']['pos_title']['id'] = 'pos_title';
$handler->display->display_options['fields']['pos_title']['table'] = 'hank_hrper';
$handler->display->display_options['fields']['pos_title']['field'] = 'pos_title';
$handler->display->display_options['fields']['pos_title']['relationship'] = 'field_administrator_target_id';
$handler->display->display_options['fields']['pos_title']['label'] = 'title';
$handler->display->display_options['fields']['pos_title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['pos_title']['hide_empty'] = TRUE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'office' => 'office',
);

/* Display: Data export */
$handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'views_data_export_xml';
$handler->display->display_options['style_options']['provide_file'] = 0;
$handler->display->display_options['style_options']['parent_sort'] = 0;
$handler->display->display_options['style_options']['transform'] = 1;
$handler->display->display_options['style_options']['root_node'] = 'offices';
$handler->display->display_options['style_options']['item_node'] = 'office';
$handler->display->display_options['style_options']['no_entity_encode'] = array(
  'title' => 'title',
  'field_phone_number' => 'field_phone_number',
  'field_email_address' => 'field_email_address',
  'field_administrator' => 'field_administrator',
  'pos_title' => 'pos_title',
);
$handler->display->display_options['path'] = 'catalog/export/offices.xml';
