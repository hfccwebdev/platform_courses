<?php

/**
 * @file
 * Defines the Course Masters view.
 */

$view = new view();
$view->name = 'course_masters';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Course Masters';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Course Masters';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'time';
$handler->display->display_options['cache']['results_lifespan'] = '300';
$handler->display->display_options['cache']['output_lifespan'] = '300';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['grouping'] = 'field_crs_div';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Division */
$handler->display->display_options['fields']['field_crs_div']['id'] = 'field_crs_div';
$handler->display->display_options['fields']['field_crs_div']['table'] = 'field_data_field_crs_div';
$handler->display->display_options['fields']['field_crs_div']['field'] = 'field_crs_div';
$handler->display->display_options['fields']['field_crs_div']['label'] = '';
$handler->display->display_options['fields']['field_crs_div']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_crs_div']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_crs_div']['hide_alter_empty'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
/* Sort criterion: Content: Division (field_crs_div) */
$handler->display->display_options['sorts']['field_crs_div_value']['id'] = 'field_crs_div_value';
$handler->display->display_options['sorts']['field_crs_div_value']['table'] = 'field_data_field_crs_div';
$handler->display->display_options['sorts']['field_crs_div_value']['field'] = 'field_crs_div_value';
/* Sort criterion: Content: Course Subject (field_crs_subj) */
$handler->display->display_options['sorts']['field_crs_subj_value']['id'] = 'field_crs_subj_value';
$handler->display->display_options['sorts']['field_crs_subj_value']['table'] = 'field_data_field_crs_subj';
$handler->display->display_options['sorts']['field_crs_subj_value']['field'] = 'field_crs_subj_value';
/* Sort criterion: Content: Course Number (field_crs_num) */
$handler->display->display_options['sorts']['field_crs_num_value']['id'] = 'field_crs_num_value';
$handler->display->display_options['sorts']['field_crs_num_value']['table'] = 'field_data_field_crs_num';
$handler->display->display_options['sorts']['field_crs_num_value']['field'] = 'field_crs_num_value';
/* Contextual filter: Content: Division (field_crs_div) */
$handler->display->display_options['arguments']['field_crs_div_value']['id'] = 'field_crs_div_value';
$handler->display->display_options['arguments']['field_crs_div_value']['table'] = 'field_data_field_crs_div';
$handler->display->display_options['arguments']['field_crs_div_value']['field'] = 'field_crs_div_value';
$handler->display->display_options['arguments']['field_crs_div_value']['default_action'] = 'summary';
$handler->display->display_options['arguments']['field_crs_div_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_crs_div_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['human'] = TRUE;
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'OR',
);
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'course_master' => 'course_master',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
/* Filter criterion: Content: Deactivation Date (field_crs_deactivation_date) */
$handler->display->display_options['filters']['field_crs_deactivation_date_value_1']['id'] = 'field_crs_deactivation_date_value_1';
$handler->display->display_options['filters']['field_crs_deactivation_date_value_1']['table'] = 'field_data_field_crs_deactivation_date';
$handler->display->display_options['filters']['field_crs_deactivation_date_value_1']['field'] = 'field_crs_deactivation_date_value';
$handler->display->display_options['filters']['field_crs_deactivation_date_value_1']['operator'] = 'empty';
$handler->display->display_options['filters']['field_crs_deactivation_date_value_1']['group'] = 2;
/* Filter criterion: Content: Deactivation Date (field_crs_deactivation_date) */
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['id'] = 'field_crs_deactivation_date_value';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['table'] = 'field_data_field_crs_deactivation_date';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['field'] = 'field_crs_deactivation_date_value';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['group'] = 2;
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['default_date'] = 'now';

/* Display: Approved Masters Page */
$handler = $view->new_display('page', 'Approved Masters Page', 'page_master');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Council-Approved Course Masters';
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Header Message';
$handler->display->display_options['header']['area']['empty'] = TRUE;
$handler->display->display_options['header']['area']['content'] = 'This list contains Course Masters which have been approved by College Council.';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['path'] = 'courses/course-masters/list';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Approved Course Masters';
$handler->display->display_options['menu']['weight'] = '-50';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Course Masters';
$handler->display->display_options['tab_options']['weight'] = '0';
$handler->display->display_options['tab_options']['name'] = 'main-menu';

/* Display: Missing Dates Page */
$handler = $view->new_display('page', 'Missing Dates Page', 'page_missing_dates');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Course Masters with Missing Dates';
$handler->display->display_options['display_description'] = 'Display published Course Masters with missing dates.';
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['cache'] = FALSE;
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_crs_div' => 'field_crs_div',
  'title' => 'title',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_crs_div' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Header Message';
$handler->display->display_options['header']['area']['content'] = 'This list contains published Course Masters that are missing approval dates. For a Course Master to be published on this system, all dates should be included.';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Course Number and Title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
/* Field: Content: Division Approval Date */
$handler->display->display_options['fields']['field_crs_div_approval']['id'] = 'field_crs_div_approval';
$handler->display->display_options['fields']['field_crs_div_approval']['table'] = 'field_data_field_crs_div_approval';
$handler->display->display_options['fields']['field_crs_div_approval']['field'] = 'field_crs_div_approval';
$handler->display->display_options['fields']['field_crs_div_approval']['label'] = 'Division Approval';
$handler->display->display_options['fields']['field_crs_div_approval']['settings'] = array(
  'format_type' => 'short_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: College Council Approval Date */
$handler->display->display_options['fields']['field_crs_cc_approval']['id'] = 'field_crs_cc_approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['table'] = 'field_data_field_crs_cc_approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['field'] = 'field_crs_cc_approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['label'] = 'College Council Approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['settings'] = array(
  'format_type' => 'short_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: Deactivation Date */
$handler->display->display_options['fields']['field_crs_deactivation_date']['id'] = 'field_crs_deactivation_date';
$handler->display->display_options['fields']['field_crs_deactivation_date']['table'] = 'field_data_field_crs_deactivation_date';
$handler->display->display_options['fields']['field_crs_deactivation_date']['field'] = 'field_crs_deactivation_date';
$handler->display->display_options['fields']['field_crs_deactivation_date']['label'] = 'Deactivation';
$handler->display->display_options['fields']['field_crs_deactivation_date']['settings'] = array(
  'format_type' => 'short_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: Effective Date */
$handler->display->display_options['fields']['field_crs_effective']['id'] = 'field_crs_effective';
$handler->display->display_options['fields']['field_crs_effective']['table'] = 'field_data_field_crs_effective';
$handler->display->display_options['fields']['field_crs_effective']['field'] = 'field_crs_effective';
$handler->display->display_options['fields']['field_crs_effective']['label'] = 'Effective';
$handler->display->display_options['fields']['field_crs_effective']['settings'] = array(
  'format_type' => 'short_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['label'] = 'Last Update';
$handler->display->display_options['fields']['changed']['date_format'] = 'short_date';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
$handler->display->display_options['defaults']['arguments'] = FALSE;
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['filter_groups']['groups'] = array(
  1 => 'AND',
  2 => 'OR',
);
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'course_master' => 'course_master',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
/* Filter criterion: Content: College Council Approval Date (field_crs_cc_approval) */
$handler->display->display_options['filters']['field_crs_cc_approval_value']['id'] = 'field_crs_cc_approval_value';
$handler->display->display_options['filters']['field_crs_cc_approval_value']['table'] = 'field_data_field_crs_cc_approval';
$handler->display->display_options['filters']['field_crs_cc_approval_value']['field'] = 'field_crs_cc_approval_value';
$handler->display->display_options['filters']['field_crs_cc_approval_value']['operator'] = 'empty';
$handler->display->display_options['filters']['field_crs_cc_approval_value']['group'] = 2;
/* Filter criterion: Content: Division Approval Date (field_crs_div_approval) */
$handler->display->display_options['filters']['field_crs_div_approval_value']['id'] = 'field_crs_div_approval_value';
$handler->display->display_options['filters']['field_crs_div_approval_value']['table'] = 'field_data_field_crs_div_approval';
$handler->display->display_options['filters']['field_crs_div_approval_value']['field'] = 'field_crs_div_approval_value';
$handler->display->display_options['filters']['field_crs_div_approval_value']['operator'] = 'empty';
$handler->display->display_options['filters']['field_crs_div_approval_value']['group'] = 2;
/* Filter criterion: Content: Effective Date (field_crs_effective) */
$handler->display->display_options['filters']['field_crs_effective_value']['id'] = 'field_crs_effective_value';
$handler->display->display_options['filters']['field_crs_effective_value']['table'] = 'field_data_field_crs_effective';
$handler->display->display_options['filters']['field_crs_effective_value']['field'] = 'field_crs_effective_value';
$handler->display->display_options['filters']['field_crs_effective_value']['operator'] = 'empty';
$handler->display->display_options['filters']['field_crs_effective_value']['group'] = 2;
$handler->display->display_options['path'] = 'courses/course-masters/missing-dates';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Missing Dates Check';
$handler->display->display_options['menu']['weight'] = '30';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;

/* Display: Recently Approved Block */
$handler = $view->new_display('block', 'Recently Approved Block', 'block_recent_approval');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Recently Approved Course Masters';
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['use_more'] = FALSE;
$handler->display->display_options['use_more'] = TRUE;
$handler->display->display_options['defaults']['use_more_always'] = FALSE;
$handler->display->display_options['defaults']['use_more_always'] = FALSE;
$handler->display->display_options['use_more_always'] = TRUE;
$handler->display->display_options['defaults']['use_more_text'] = FALSE;
$handler->display->display_options['use_more_text'] = 'view complete list';
$handler->display->display_options['defaults']['pager'] = FALSE;
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['default_field_elements'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Block description';
$handler->display->display_options['header']['area']['content'] = 'The following course masters were recently approved by College Council.';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_type'] = 'strong';
$handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
/* Field: Content: College Council Approval Date */
$handler->display->display_options['fields']['field_crs_cc_approval']['id'] = 'field_crs_cc_approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['table'] = 'field_data_field_crs_cc_approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['field'] = 'field_crs_cc_approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['label'] = 'College Council Approval';
$handler->display->display_options['fields']['field_crs_cc_approval']['settings'] = array(
  'format_type' => 'short_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: College Council Approval Date (field_crs_cc_approval) */
$handler->display->display_options['sorts']['field_crs_cc_approval_value']['id'] = 'field_crs_cc_approval_value';
$handler->display->display_options['sorts']['field_crs_cc_approval_value']['table'] = 'field_data_field_crs_cc_approval';
$handler->display->display_options['sorts']['field_crs_cc_approval_value']['field'] = 'field_crs_cc_approval_value';
$handler->display->display_options['sorts']['field_crs_cc_approval_value']['order'] = 'DESC';
/* Sort criterion: Content: Updated date */
$handler->display->display_options['sorts']['changed']['id'] = 'changed';
$handler->display->display_options['sorts']['changed']['table'] = 'node';
$handler->display->display_options['sorts']['changed']['field'] = 'changed';
$handler->display->display_options['sorts']['changed']['order'] = 'DESC';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Division (field_crs_div) */
$handler->display->display_options['arguments']['field_crs_div_value']['id'] = 'field_crs_div_value';
$handler->display->display_options['arguments']['field_crs_div_value']['table'] = 'field_data_field_crs_div';
$handler->display->display_options['arguments']['field_crs_div_value']['field'] = 'field_crs_div_value';
$handler->display->display_options['arguments']['field_crs_div_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_crs_div_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['human'] = TRUE;
$handler->display->display_options['arguments']['field_crs_div_value']['limit'] = '0';

/* Display: Deactivated Page */
$handler = $view->new_display('page', 'Deactivated Page', 'page_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Deactivated Course Masters';
$handler->display->display_options['display_description'] = 'Display a list of deactivated course masters.';
$handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_crs_div' => 'field_crs_div',
  'title' => 'title',
  'field_crs_deactivation_date' => 'field_crs_deactivation_date',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_crs_div' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_crs_deactivation_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_crs_effective' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Header Message';
$handler->display->display_options['header']['area']['content'] = 'This list contains Course Masters that have been deactivated since September 2012. For previous history, please see the College Council Year in Review documents on the G: drive.';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Content: Division */
$handler->display->display_options['fields']['field_crs_div']['id'] = 'field_crs_div';
$handler->display->display_options['fields']['field_crs_div']['table'] = 'field_data_field_crs_div';
$handler->display->display_options['fields']['field_crs_div']['field'] = 'field_crs_div';
$handler->display->display_options['fields']['field_crs_div']['label'] = '';
$handler->display->display_options['fields']['field_crs_div']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_crs_div']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_crs_div']['hide_alter_empty'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
/* Field: Content: Deactivation Date */
$handler->display->display_options['fields']['field_crs_deactivation_date']['id'] = 'field_crs_deactivation_date';
$handler->display->display_options['fields']['field_crs_deactivation_date']['table'] = 'field_data_field_crs_deactivation_date';
$handler->display->display_options['fields']['field_crs_deactivation_date']['field'] = 'field_crs_deactivation_date';
$handler->display->display_options['fields']['field_crs_deactivation_date']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_crs_deactivation_date']['settings'] = array(
  'format_type' => 'short_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: Effective Date */
$handler->display->display_options['fields']['field_crs_effective']['id'] = 'field_crs_effective';
$handler->display->display_options['fields']['field_crs_effective']['table'] = 'field_data_field_crs_effective';
$handler->display->display_options['fields']['field_crs_effective']['field'] = 'field_crs_effective';
$handler->display->display_options['fields']['field_crs_effective']['settings'] = array(
  'format_type' => 'short_date',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Effective Date (field_crs_effective) */
$handler->display->display_options['sorts']['field_crs_effective_value']['id'] = 'field_crs_effective_value';
$handler->display->display_options['sorts']['field_crs_effective_value']['table'] = 'field_data_field_crs_effective';
$handler->display->display_options['sorts']['field_crs_effective_value']['field'] = 'field_crs_effective_value';
$handler->display->display_options['sorts']['field_crs_effective_value']['order'] = 'DESC';
/* Sort criterion: Content: Deactivation Date (field_crs_deactivation_date) */
$handler->display->display_options['sorts']['field_crs_deactivation_date_value']['id'] = 'field_crs_deactivation_date_value';
$handler->display->display_options['sorts']['field_crs_deactivation_date_value']['table'] = 'field_data_field_crs_deactivation_date';
$handler->display->display_options['sorts']['field_crs_deactivation_date_value']['field'] = 'field_crs_deactivation_date_value';
$handler->display->display_options['sorts']['field_crs_deactivation_date_value']['order'] = 'DESC';
/* Sort criterion: Content: Course Subject (field_crs_subj) */
$handler->display->display_options['sorts']['field_crs_subj_value']['id'] = 'field_crs_subj_value';
$handler->display->display_options['sorts']['field_crs_subj_value']['table'] = 'field_data_field_crs_subj';
$handler->display->display_options['sorts']['field_crs_subj_value']['field'] = 'field_crs_subj_value';
/* Sort criterion: Content: Course Number (field_crs_num) */
$handler->display->display_options['sorts']['field_crs_num_value']['id'] = 'field_crs_num_value';
$handler->display->display_options['sorts']['field_crs_num_value']['table'] = 'field_data_field_crs_num';
$handler->display->display_options['sorts']['field_crs_num_value']['field'] = 'field_crs_num_value';
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Content: Division (field_crs_div) */
$handler->display->display_options['arguments']['field_crs_div_value']['id'] = 'field_crs_div_value';
$handler->display->display_options['arguments']['field_crs_div_value']['table'] = 'field_data_field_crs_div';
$handler->display->display_options['arguments']['field_crs_div_value']['field'] = 'field_crs_div_value';
$handler->display->display_options['arguments']['field_crs_div_value']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_crs_div_value']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_crs_div_value']['summary']['human'] = TRUE;
$handler->display->display_options['arguments']['field_crs_div_value']['limit'] = '0';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'course_master' => 'course_master',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
$handler->display->display_options['filters']['status']['group'] = 1;
/* Filter criterion: Content: Deactivation Date (field_crs_deactivation_date) */
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['id'] = 'field_crs_deactivation_date_value';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['table'] = 'field_data_field_crs_deactivation_date';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['field'] = 'field_crs_deactivation_date_value';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['operator'] = '<';
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['group'] = 1;
$handler->display->display_options['filters']['field_crs_deactivation_date_value']['default_date'] = 'now';
$handler->display->display_options['path'] = 'courses/course-masters/inactive';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Deactivated Courses';
$handler->display->display_options['menu']['weight'] = '25';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Course Masters';
$handler->display->display_options['tab_options']['weight'] = '0';
$handler->display->display_options['tab_options']['name'] = 'main-menu';
