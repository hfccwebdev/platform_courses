<?php

/**
 * @file
 * Provides support for Views integration.
 */

/**
 * Implements hook_views_data_alter().
 */
function catdivcodes_views_data_alter(&$data) {
  // Add a dummy field on field_data_field_crs_subj that can generate
  // information about faked division codes based on subject.
  $data['field_data_field_crs_subj']['catalog_div_id'] = array(
    'title' => t('Catalog Division Code'),
    'group' => t('HFC Catalog'),
    'help' => t('Division code to display for subject.'),
    'real field' => 'field_crs_subj_value',
    'field' => array(
      'handler' => 'catdivcodes_handler_field_catalog_div_id',
      'click sortable' => TRUE,
    ),
  );
}
