<?php

/**
 * @file
 * Defines the Catalog Division ID views field handler.
 */

/**
 * Field handler to display the catalog division code.
 *
 * @ingroup views_field_handlers
 */
class catdivcodes_handler_field_catalog_div_id extends views_handler_field {

  /**
   * Add the custom table into the query.
   */
  function query() {
    $this->ensure_my_table();
    // Add the field.
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array();

    // Set up the join to the custom table so we can get the catalog code.
    $join = new views_join;
    $join->construct('catdivcodes_subjects', $this->table_alias, $this->real_field, 'subjects_id');
    $this->query->add_relationship('catdivs', $join, 'catdivcodes_subjects');

    // Query the custom table instead of the actual value.
    $this->field_alias = $this->query->add_field('catdivs', 'catalog_div_id', NULL, $params);

    $this->add_additional_fields();
  }

}
