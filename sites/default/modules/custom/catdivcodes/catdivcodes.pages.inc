<?php

/**
 * @file
 * Contains page callbacks for catdivcodes module.
 *
 * @see catdivcodes.module
 */

/**
 * Page callback for subject to division dump.
 */
function catdivcodes_subject_divs() {
  drupal_add_http_header('Content-Type', 'text/plain; charset=utf-8');
  $query = db_select('field_data_field_crs_subj', 's')
    ->condition('s.entity_type', 'node')
    ->condition('s.bundle', 'course_master')
    ;
  $query->addField('s', 'field_crs_subj_value', 'subjects_id');

  $query->join('field_data_field_crs_div', 'd', "s.entity_id = d.entity_id AND s.revision_id = d.revision_id AND d.entity_type = 'node'");
  $query->addField('d', 'field_crs_div_value', 'hank_division_id');

  $query->leftJoin('catdivcodes_subjects', 'c', "s.field_crs_subj_value = c.subjects_id");
  $query->addField('c', 'catalog_div_id', 'catalog_div_id');

  $query->groupBy('s.field_crs_subj_value');
  $query->groupBy('d.field_crs_div_value');
  $query->orderBy('s.field_crs_subj_value');
  $query->orderBy('d.field_crs_div_value');

  $result = $query->execute()->fetchAll();

  echo t("subjects_id,hank_division_id,catalog_div_id\n");

  foreach ($result as $item) {
    echo t("@s,@d,@c\n", array(
      '@s' => $item->subjects_id,
      '@d' => $item->hank_division_id,
      '@c' => $item->catalog_div_id,
    ));
  }

  // Drop out of Drupal. We are done here.
  drupal_exit();
}
